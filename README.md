# prova.automacao.api

_Projeto com suite de testes de uma api para consultar restrições e mexer com simulações._


- Java :8
- Apache Maven: 3.8.1


**Principai bibliotecas:**

- RestAssured
- Junit
- cucumber


**Execuçao do testes:**
 Realizar através da classe executa pelo junit, configurar qual teste quer realizar através das tags @executa para executar e @nexcuta para não executar dentro das features. 

 **Cucumber reporting:**
 Após executar o teste escolhido, utilizar o comando mvn verify -DskipTests no terminal para gerar o cucumber report. Para gerar outros relatorios rodar o comando mvn clean install, executar o teste escolhido e depois novamente o comando  mvn verify -DskipTests. O relatorio estara na pasta target/cucumber-hmtl-reports.
