package prova.automacao.api.metodos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.hamcrest.core.StringContains;
import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Metodos {
	RequestSpecification request = RestAssured.given();
	Response response;

	// verificar em qual porta esta rodando sua api
	String baseURI = "http://localhost:8888/api/v1/";

	public Response GET(String caminhoGET) {

		response = request.get(baseURI + caminhoGET);
		return response;

	}

	public Response POST(String caminhoPOST) {

		response = request.post(baseURI + caminhoPOST);
		return response;

	}

	public Response PUT(String caminhoPUT) {

		response = request.put(baseURI + caminhoPUT);
		return response;

	}

	public Response DELETE(String caminhoDELETE) {

		response = request.delete(baseURI + caminhoDELETE);
		return response;

	}

	public RequestSpecification corpoDeRequisicao(String nome, String cpf, String email, int valor, int parcelas,
			boolean seguro) {

		request.header("Content-Type", "application/json");

		JSONObject json = new JSONObject();

		json.put("nome", nome);
		json.put("cpf", cpf);
		json.put("email", email);
		json.put("valor", valor);
		json.put("parcelas", parcelas);
		json.put("seguro", seguro);

		return request.body(json.toJSONString());

	}

	public void codigoDeEstado(int codigo) {

		int estadoDeCodigo = response.getStatusCode();
		assertEquals(codigo, estadoDeCodigo);

	}

	

	public void exibirCorpoDeResposta() {

		String corpo = response.asPrettyString();
		System.out.println();
		System.out.println("******************************************");
		System.out.println();
		System.out.println(corpo);
		System.out.println();
		System.out.println("******************************************");
		System.out.println();

	}

	public void validarCorpoDeResposta(String corpoComoString) {
		
		String corpo = response.asPrettyString();

		assertThat(corpo, StringContains.containsString(corpoComoString));

	}


}
