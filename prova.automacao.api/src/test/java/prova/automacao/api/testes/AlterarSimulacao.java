package prova.automacao.api.testes;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import prova.automacao.api.metodos.Metodos;
import prova.automacao.api.valoresRandomicos.ValoresRandomicos;

public class AlterarSimulacao {
	
	Metodos metodo = new Metodos(); 
	ValoresRandomicos valorRandomico = new ValoresRandomicos();
	
	@Dado("que ao trocar informacoes do corpo de requisicao")
	public void que_ao_trocar_informacoes_do_corpo_de_requisicao() {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(),"66414919004", valorRandomico.gerarEmail(),
				valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}
	
	@Dado("que esteja usando o verbo post com o parametro de um cpf {string} invalido")
	public void que_esteja_usando_o_verbo_post_com_o_parametro_de_um_cpf_invalido(String cpfInvalido) {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(),valorRandomico.gerarCPF(), valorRandomico.gerarEmail(),
				valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
		metodo.PUT("simulacoes/"+cpfInvalido);
	}


	@Quando("esteja usando o verbo post com o parametro de um cpf {string} valido")
	public void esteja_usando_o_verbo_post_com_o_parametro_de_um_cpf_valido(String cpfValido) {
		metodo.PUT("simulacoes/"+cpfValido);
	}

	@Entao("verifico se o codigo de estado e ok {int}")
	public void verifico_se_o_codigo_de_estado_e_ok(Integer codigo200) {
	  metodo.codigoDeEstado(codigo200);
	}
	
	@Entao("valido se o estado de codigo e {int}")
	public void valido_se_o_estado_de_codigo_e(Integer codigo404) {
	metodo.codigoDeEstado(codigo404);
	}

	@Entao("se o corpo de resposta corresponde a alteracao")
	public void se_o_corpo_de_resposta_corresponde_a_alteracao() {
		metodo.exibirCorpoDeResposta();
		metodo.validarCorpoDeResposta("nome");
		metodo.validarCorpoDeResposta("cpf");
		metodo.validarCorpoDeResposta("email");
		metodo.validarCorpoDeResposta("valor");
		metodo.validarCorpoDeResposta("parcelas"); 
		metodo.validarCorpoDeResposta("seguro");
	}

	@Entao("com a mensagem {string}")
	public void com_a_mensagem(String cpfNaoEncontrado) {
	    metodo.validarCorpoDeResposta("CPF 19626829002 não encontrado");
	}


}
