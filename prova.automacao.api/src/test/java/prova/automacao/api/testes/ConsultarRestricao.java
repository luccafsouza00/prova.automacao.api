package prova.automacao.api.testes;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import prova.automacao.api.metodos.Metodos;
import prova.automacao.api.valoresRandomicos.ValoresRandomicos;

public class ConsultarRestricao {
	
	Metodos metodo = new Metodos(); 
	ValoresRandomicos valorRandomico = new ValoresRandomicos();
	
	
	@Dado("que esteja usando o verbo get para consultar {string}")
	public void que_esteja_usando_o_verbo_get_para_consultar(String cpfComRestricao) {
	  metodo.GET("restricoes/"+cpfComRestricao);
	}
	
	@Dado("que esteja usando o verbo get para consultar um cpf {string} sem resticao")
	public void que_esteja_usando_o_verbo_get_para_consultar_um_cpf_sem_resticao(String cpfSemRestricao) {
	    metodo.GET("restricoes/"+cpfSemRestricao);
	}
	
	@Entao("verifico se o codigo de estado e {int}")
	public void verifico_se_o_codigo_de_estado_e(Integer codigo) {
	    metodo.codigoDeEstado(codigo);
	}
	
	@Entao("verifico a mensagem de restricao com o cpf {string}")
	public void verifico_a_mensagem_de_restricao_com_o_cpf(String cpfComRestricao) {
	    metodo.validarCorpoDeResposta("O CPF "+cpfComRestricao+" tem problema");
	}
	
	
}
