package prova.automacao.api.testes;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import prova.automacao.api.metodos.Metodos;
import prova.automacao.api.valoresRandomicos.ValoresRandomicos;

public class RemoverSimulacao {
	
	Metodos metodo = new Metodos(); 
	ValoresRandomicos valorRandomico = new ValoresRandomicos();
	
	@Dado("que esteja usando o verbo delete passando o caminho de id {int}  existente")
	public void que_esteja_usando_o_verbo_delete_passando_o_caminho_de_id_existente(Integer idExistente) {
	    metodo.DELETE("simualcoes/"+idExistente);
	}
	
	@Dado("que esteja usando o verbo delete passando um caminho de id {int} inexistente")
	public void que_esteja_usando_o_verbo_delete_passando_um_caminho_de_id_inexistente(Integer idInexistente) {
		 metodo.DELETE("simualcoes/"+idInexistente);
	}

	@Entao("valido se o status code e {int}")
	public void valido_se_o_status_code_e(Integer codigo) {
		metodo.codigoDeEstado(codigo);
	}

	@Entao("se a mensagem e {string}")
	public void se_a_mensagem_e(String simulacaoNaoEncontrada) {
		metodo.validarCorpoDeResposta(simulacaoNaoEncontrada);
	}

}
