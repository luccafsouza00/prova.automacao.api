package prova.automacao.api.testes;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import prova.automacao.api.metodos.Metodos;
import prova.automacao.api.valoresRandomicos.ValoresRandomicos;

public class CriarSimulacao {
	
	Metodos metodo = new Metodos(); 
	ValoresRandomicos valorRandomico = new ValoresRandomicos();
	
	@Dado("que se passe os atributos corretos")
	public void que_se_passe_os_atributos_corretos() {
	    metodo.corpoDeRequisicao(valorRandomico.gerarNome(), valorRandomico.gerarCPF(), valorRandomico.gerarEmail(), 
	    		                 valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}
	

	@Dado("que ao inserir os atributos com um valor errado no campo nome {string}")
	public void que_ao_inserir_os_atributos_com_um_valor_errado_no_campo_nome(String nomeErrado) {
		metodo.corpoDeRequisicao(nomeErrado, valorRandomico.gerarCPF(), valorRandomico.gerarEmail(), 
                valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Dado("que ao inserir os atributos com um formato nao valido de cpf {string}")
	public void que_ao_inserir_os_atributos_com_um_formato_nao_valido_de_cpf(String cpfInvalido) {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(), cpfInvalido, valorRandomico.gerarEmail(), 
                valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Dado("que ao inserir os atributos com um texto de email {string} nao valido")
	public void que_ao_inserir_os_atributos_com_um_texto_de_email_nao_valido(String emailErrado) {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(), valorRandomico.gerarCPF(), emailErrado, 
                valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Dado("que ao inserir os atributos com um valor {int} fora do intervalo valido")
	public void que_ao_inserir_os_atributos_com_um_valor_fora_do_intervalo_valido(Integer valorErrado) {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(), valorRandomico.gerarCPF(), valorRandomico.gerarEmail(), 
				valorErrado, valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Dado("que ao inserir os atributos com um numero de parcelas {int} fora do intervalo valido")
	public void que_ao_inserir_os_atributos_com_um_numero_de_parcelas_fora_do_intervalo_valido(Integer parcelasErradas) {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(), valorRandomico.gerarCPF(), valorRandomico.gerarEmail(), 
                valorRandomico.gerarValor(), parcelasErradas, valorRandomico.gerarSeguro());
	}

	@Dado("que nao se passe algum atributo")
	public void que_nao_se_passe_algum_atributo() {
		metodo.corpoDeRequisicao(null, valorRandomico.gerarCPF(), valorRandomico.gerarEmail(), 
                valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Dado("que se passe os atributos com um cpf ja existente")
	public void que_se_passe_os_atributos_com_um_cpf_ja_existente() {
		metodo.corpoDeRequisicao(valorRandomico.gerarNome(),"66414919003", valorRandomico.gerarEmail(), 
                valorRandomico.gerarValor(), valorRandomico.gerarParcelas(), valorRandomico.gerarSeguro());
	}

	@Quando("usar o verbo post para criar simulacoes")
	public void usar_o_verbo_post_para_criar_simulacoes() {
	   metodo.POST("simulacoes/");
	}

	@Entao("valido se o codigo de estado e {int}")
	public void valido_se_o_codigo_de_estado_e(Integer codigo) {
	 metodo.codigoDeEstado(codigo);
	}
	
	@Entao("valido as informacoes cadastradas no corpo de resposta")
	public void valido_as_informacoes_cadastradas_no_corpo_de_resposta() {
	   metodo.exibirCorpoDeResposta();
		metodo.validarCorpoDeResposta("nome");
		metodo.validarCorpoDeResposta("cpf");
		metodo.validarCorpoDeResposta("email");
		metodo.validarCorpoDeResposta("valor");
		metodo.validarCorpoDeResposta("parcelas"); 
		metodo.validarCorpoDeResposta("seguro");
	}

	@Entao("valido a lista de erros")
	public void valido_a_lista_de_erros() {
	    metodo.validarCorpoDeResposta("erros");
	}

	@Entao("valido a mensagem de erro {string}")
	public void valido_a_mensagem_de_erro(String cpfNaoEncontrado) {
	    metodo.validarCorpoDeResposta(cpfNaoEncontrado);
	}

}
