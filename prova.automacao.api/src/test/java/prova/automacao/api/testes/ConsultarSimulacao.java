package prova.automacao.api.testes;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import prova.automacao.api.metodos.Metodos;
import prova.automacao.api.valoresRandomicos.ValoresRandomicos;

public class ConsultarSimulacao {
	
	Metodos metodo = new Metodos(); 
	ValoresRandomicos valorRandomico = new ValoresRandomicos();
	
	@Dado("que esteja usando o verbo get para consultar todas as simulacoes")
	public void que_esteja_usando_o_verbo_get_para_consultar_todas_as_simulacoes() {
		metodo.GET("simulacoes");
	}
	
	@Dado("que esteja usando o verbo get com o caminho de um cpf {string} para consultar uma simulacoes especifica")
	public void que_esteja_usando_o_verbo_get_com_o_caminho_de_um_cpf_para_consultar_uma_simulacoes_especifica(String cpf) {
		metodo.GET("simulacoes/" + cpf);
	}
	
	@Dado("que esteja usando o verbo get com o caminho de um cpf inexistente {string} para consultar uma simulacoes especifica")
	public void que_esteja_usando_o_verbo_get_com_o_caminho_de_um_cpf_inexistente_para_consultar_uma_simulacoes_especifica(String cpfInexistente) {
		metodo.GET("simulacoes/"+cpfInexistente);
	}

	@Entao("valido se o codido de estado e {int}")
	public void valido_se_o_codido_de_estado_e(Integer codigo) {
	   metodo.codigoDeEstado(codigo);
	}

	@Entao("se retorna a lista de simulacoes cadastradas")
	public void se_retorna_a_lista_de_simulacoes_cadastradas() {
	   metodo.exibirCorpoDeResposta();
	}

	@Entao("valido se retorna a simulacao cadastrada")
	public void valido_se_retorna_a_simulacao_cadastrada() {
		metodo.exibirCorpoDeResposta();
		metodo.validarCorpoDeResposta("nome");
		metodo.validarCorpoDeResposta("cpf");
		metodo.validarCorpoDeResposta("email");
		metodo.validarCorpoDeResposta("valor");
		metodo.validarCorpoDeResposta("parcelas"); 
		metodo.validarCorpoDeResposta("seguro");
		
	}


	//metodo.GET("simulacoes");



//	metodo.GET("simulacoes/" + cpf);



//   metodo.GET("simulacoes/"+cpfInexistente);



	//	metodo.statusCod(code);



/**	metodo.bodyResponse();
	metodo.validarBodyResponse("nome");
	metodo.validarBodyResponse("cpf");
	metodo.validarBodyResponse("email"); 
	metodo.validarBodyResponse("valor");
	metodo.validarBodyResponse("parcelas"); 
	metodo.validarBodyResponse("seguro");**/




/**	metodo.bodyResponse();
	metodo.validarBodyResponse("nome");
	metodo.validarBodyResponse("cpf");
	metodo.validarBodyResponse("email");
	metodo.validarBodyResponse("valor");
	metodo.validarBodyResponse("parcelas"); 
	metodo.validarBodyResponse("seguro");**/

	

	


}
