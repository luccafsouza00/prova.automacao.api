package prova.automacao.api.valoresRandomicos;

import java.util.Random;

public class ValoresRandomicos {
	
	private static Random rand = new Random();
	private static char[] numeros = "0123456789".toCharArray();
	private static char[] letras = "abcdefghijlmnopqrstuvxz".toCharArray();

	public String gerarCPF() {
		StringBuffer generatedString = new StringBuffer();
		for (int i = 0; i < 11; i++) {
			int ch = rand.nextInt(numeros.length);
			generatedString.append(numeros[ch]);
		}

		return generatedString.toString();

	}
	
	public String gerarNome() {
		StringBuffer generatedString = new StringBuffer();
		for (int i = 0; i < 8; i++) { 
			int ch = rand.nextInt(letras.length);
			generatedString.append(letras[ch]);
		}
  
		return generatedString.toString();

	}

	public String gerarEmail() {
		StringBuffer generatedString = new StringBuffer();
		for (int i = 0; i < 8; i++) {
			int ch = rand.nextInt(letras.length);
			generatedString.append(letras[ch]);
		}

		return (generatedString.toString() + "@email.com");

	}


	public int gerarValor() {

		return rand.nextInt(39001) + 1000;
	}

	public int gerarParcelas() {

		return rand.nextInt(47) + 2;

	}

	public boolean gerarSeguro() {

		return rand.nextBoolean();

	}

}
