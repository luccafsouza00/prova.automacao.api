#Author: luccafsouza00l@gmail.com
# language: pt

@nexecuta
Funcionalidade: Atualizar simulacoes
 Como api quero alterar dados de simulacoes atraves do cpf


  @atualizar
  	Esquema do Cenario: Atualizar atributos da simulacao com sucesso
    Dado que ao trocar informacoes do corpo de requisicao  
    Quando esteja usando o verbo post com o parametro de um cpf <cpfValido> valido
    Entao verifico se o codigo de estado e ok 200
    E se o corpo de resposta corresponde a alteracao
    
     Exemplos: 
    |       cpfValido     |
    |   "66414919004"     |
    
   @atualizar
    Esquema do Cenario: Atualizar simulacao com cpf sem simulacao
    Dado que esteja usando o verbo post com o parametro de um cpf <cpfInvalido> invalido
    Entao valido se o estado de codigo e 404
    E com a mensagem "CPF não encontrado"
    
    Exemplos:
    | cpfInvalido   |
    |"19626829002"  |
    
