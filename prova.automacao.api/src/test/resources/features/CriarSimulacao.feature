#Author: luccafsouza00l@gmail.com
# language: pt

@nexecuta
Funcionalidade: Criar simulacoes
  Como api quero criar simulacoes

  @criar
  Cenario: Simulacao criada com sucesso
    Dado que se passe os atributos corretos  
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 201
    E valido as informacoes cadastradas no corpo de resposta
    
     @criar
    Esquema do Cenario: Simulacao com erro no nome 
    Dado que ao inserir os atributos com um valor errado no campo nome <nome>
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erros
    
    Exemplos: 
      |     nome      |   
      | "97093236014" |  
      | "@ $ˆˆˆ("     |
    
    @criar
    Esquema do Cenario: Simulacao com erro no cpf
    Dado que ao inserir os atributos com um formato nao valido de cpf <cpf> 
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erros
    
    Exemplos: 
      |     cpf           |   
      |          "23"     |  
      | "672.253.578.-17" | 
      
   	 
  @criar
    Esquema do Cenario: Simulacao com erro no email
    Dado que ao inserir os atributos com um texto de email <email> nao valido
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erros
    
    Exemplos: 
      |     email     | 
      | "email"       |  
      | "600990"      | 
     
    
  @criar
    Esquema do Cenario: Simulacao com erro no valor
    Dado que ao inserir os atributos com um valor <valor> fora do intervalo valido
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erros
    
    Exemplos: 
      |     valor     |   
      |     999       |  
      |     40001     | 
      
      
    
    @criar
    Esquema do Cenario: Simulacao com erro no numero de parcelas 
    Dado que ao inserir os atributos com um numero de parcelas <parcelas> fora do intervalo valido
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erros
    
    Exemplos: 
      |     parcelas       |   
      |       1            |  
      |       49           | 
      
    @criar
    Cenario: Simulacao sem passar algum atributo 
    Dado que nao se passe algum atributo 
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 400
    E valido a lista de erro
      
     
    @criar
    Cenario: Simulacao com cpf ja existente 
    Dado que se passe os atributos com um cpf ja existente
    Quando usar o verbo post para criar simulacoes 
    Entao valido se o codigo de estado e 409
    E valido a mensagem de erro "CPF já existente"
    
   