#Author: luccafsouza00l@gmail.com
# language: pt

@nexecuta
Funcionalidade: Consultar simulacoes
 Como api quero consultar simulacoes 

  @consultar
  Cenario: Consultar todas as simulacoes 
    Dado que esteja usando o verbo get para consultar todas as simulacoes
    Entao valido se o codido de estado e 200 
    E se retorna a lista de simulacoes cadastradas
    
    @consultar
  Esquema do Cenario: Consultar uma simulacao
    Dado que esteja usando o verbo get com o caminho de um cpf <cpf> para consultar uma simulacoes especifica
    Entao valido se o codido de estado e 200  
    E valido se retorna a simulacao cadastrada
    
    Exemplos:
    
    |    cpf      |
    |"66414919003"|
    |"17822386034"|
   
   
  @consultar
  Esquema do Cenario: Consultar uma simulacao nao existente
    Dado que esteja usando o verbo get com o caminho de um cpf inexistente <cpfInexistente> para consultar uma simulacoes especifica 
    Entao valido se o codido de estado e 404
    
   
  Exemplos:
   
    |    cpfInexistente      |
    |            "1"         |
    |            "2"         |
    |            "3"         |

