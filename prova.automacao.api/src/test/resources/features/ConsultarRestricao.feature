#Author: luccafsouza00@gmail.com
# language: pt

@executa
Funcionalidade: Consulta de CPFs
  Como api quero consultar CPFs para validar se existe alguma restricao

@restricao
  Esquema do Cenario: Consulta de CPFs com restricao
    Dado que esteja usando o verbo get para consultar <cpf>
    Entao verifico se o codigo de estado e 200
    E verifico a mensagem de restricao com o cpf <cpf>
    
    
    Exemplos: 
      |     cpf       |   
      | "97093236014" |  
      | "60094146012" | 
      | "84809766080" | 
      | "62648716050" |
      | "26276298085" | 
      | "01317496094" | 
      | "55856777050" | 
      | "19626829001" | 
      | "24094592008" | 
      | "58063164083" |
      
      
@restricao
  Cenario: Consulta de CPFs sem restricao
    Dado que esteja usando o verbo get para consultar um cpf <cpf> sem resticao
    Entao verifico se o codigo de estado e 204 

    Exemplos:
    |     cpf     |
    |"66414919003"|
    |"17822386034"|