#Author: luccafsouza00l@gmail.com
# language: pt

@nexecuta
Funcionalidade: Remover simulacao
  Como api quero remover simulacao cadastrada atraves do ID


@remover
  Esquema do Cenario: Remover com sucesso
    Dado que esteja usando o verbo delete passando o caminho de id <id> existente
    Entao valido se o status code e 200
    
     Exemplos: 
      |     id        |   
      |     13        |  
      |     14        | 
   
      
    @remover
  Esquema do Cenario: Remover sem sucesso
    Dado que esteja usando o verbo delete passando um caminho de id <idInvalido> inexistente
    Entao valido se o status code e 404
    E valido se a mensagem e "Simulação não encontrada"
    
     Exemplos: 
      |     idInvalido       |   
      |       35000          |  
      |       34000          | 
     
